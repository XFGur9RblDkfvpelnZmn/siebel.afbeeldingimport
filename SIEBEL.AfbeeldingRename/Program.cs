﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using System.Data.Sql;
namespace SIEBEL.AfbeeldingRename
{
    class Program
    {
        static void Main(string[] args)
        {
            int iArg = 0;
            if (args.Length > 0 && Int32.TryParse(args[0], out iArg))
            {
                string sConnectionString = Properties.Settings.Default.connectionString[iArg];
                FileInfo fiLoggingFile = new FileInfo(Properties.Settings.Default.loggingFile[iArg]);
                string sLoggingFile = fiLoggingFile.FullName.Split('.')[0] + DateTime.Now.ToString("yyyyMMdd") +"."+ fiLoggingFile.FullName.Split('.')[1];
                string sPictureMap = Properties.Settings.Default.pictureMap[iArg];
                string sPictureMapRejected = Properties.Settings.Default.pictureMapRejected[iArg];
                string sPictureMapProcessed = Properties.Settings.Default.pictureMapProcessed[iArg];

                if (Directory.Exists(sPictureMap) && Directory.Exists(sPictureMapProcessed))
                {
                    using (SqlConnection myConnection = new SqlConnection(sConnectionString))
                    {
                        try
                        {
                            myConnection.Open();

                            foreach (string sFile in Directory.GetFiles(sPictureMap, "*.jpg"))
                            {
                                try
                                {
                                    string sFileName = Path.GetFileNameWithoutExtension(sFile);
                                    string sCodeArticle = sFileName.Substring(0, 9);
                                    byte[] bImage = File.ReadAllBytes(sFile);
                                    using (System.Data.SqlClient.SqlCommand sqlCommand = myConnection.CreateCommand())
                                    {
                                        
                                        sqlCommand.CommandText = "SELECT GAT_REFARTICLE FROM ARTICLETIERS WHERE GAT_TYPREFARTTIERS = 'CLI' AND GAT_REFTIERS = '15100027' AND '" + sFileName + "' = SUBSTRING(GAT_ARTICLE, 1, 9)";
                                        StreamWriter sw = new StreamWriter(sLoggingFile, true);
                                        sw.WriteLine(sqlCommand.CommandText);
                                        sw.Close();
                                        string sFileRename = (string)sqlCommand.ExecuteScalar();
                                        if (sFileRename.Trim().Length == 0)
                                        {
                                            sFileRename = sFileName + ".jpg";
                                        }
                                        File.Move(sFile, sPictureMapProcessed + "\\" + sFileRename + ".jpg");

                                        sw = new StreamWriter(sLoggingFile, true);
                                        sw.WriteLine("File " + sFileName + ".jpg processed");

                                        sw.Close();
                                    }
                                    //SELECT GA_ARTICLE FROM ARTICLE WHERE GA_CODEARTICLE = 'artikelnummer uit bestandsnaam' AND GA_STATUTART IN ('GEN','UNI').
                                }
                                catch (Exception ex)
                                {

                                    string sException = "Something went wrong with " + sFile;
                                    Console.WriteLine(sException);
                                    Console.WriteLine(ex.ToString());

                                    StreamWriter sw = new StreamWriter(sLoggingFile, true);
                                    sw.WriteLine(sException);
                                    sw.WriteLine(ex.ToString());
                                    sw.Close();
                                    try
                                    {
                                        File.Move(sFile, sPictureMapRejected + "\\" + Path.GetFileNameWithoutExtension(sFile) + ".jpg");
                                    }
                                    catch (Exception ex2)
                                    {
                                        string sException2 = "Move to rejected also failed";
                                        Console.WriteLine(sException);
                                        Console.WriteLine(ex2.ToString());

                                        sw = new StreamWriter(sLoggingFile);
                                        sw.WriteLine(sException2);
                                        sw.WriteLine(ex2.ToString());
                                        sw.Close();
                                    }
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            string sException = "Could not open DB connection";
                            Console.WriteLine(sException);
                            Console.WriteLine(ex.ToString());
                            StreamWriter sw = new StreamWriter(sLoggingFile, true);
                            sw.WriteLine(sException);
                            sw.WriteLine(ex.ToString());
                            sw.Close();

                        }
                    }
                }
                else
                {
                    string sException = "Incorrect location specified";
                    Console.WriteLine(sException);
                }
                string sPattern = fiLoggingFile.Name.Split('.')[0]+"*."+fiLoggingFile.FullName.Split('.')[1];
                try
                {
                    foreach (string sFile in Directory.GetFiles(fiLoggingFile.Directory.ToString(), sPattern))
                    {
                        FileInfo fi = new FileInfo(sFile);
                        if (fi.CreationTime < DateTime.Now.AddDays(-7))
                        {
                            try
                            {
                                fi.Delete();
                            }
                            catch (Exception ex)
                            {
                                string sException = "Could not delete logging file";
                                StreamWriter sw = new StreamWriter(sLoggingFile, true);
                                sw.WriteLine(sException);
                                sw.WriteLine(ex.ToString());
                                sw.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    StreamWriter sw = new StreamWriter(sLoggingFile, true);
                    sw.WriteLine(ex.ToString());
                    sw.Close();
                }
            }
            else
            {
                string sException = "Integer argument needs to be specified";
                Console.WriteLine(sException);
            }
            
        }
    }
}
