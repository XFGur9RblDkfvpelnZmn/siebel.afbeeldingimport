﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace SIEBEL.AfbeeldingImport_2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            int iArg = 0;
            if (args.Length > 0 && Int32.TryParse(args[0], out iArg))
            {
                string sConnectionString = Properties.Settings.Default.connectionString[iArg];
                string sLoggingFile = Properties.Settings.Default.loggingFile[iArg];
                string sPictureMap = Properties.Settings.Default.pictureMap[iArg];
                string sPictureMapRejected = Properties.Settings.Default.pictureMapRejected[iArg];
                string sPictureMapProcessed = Properties.Settings.Default.pictureMapProcessed[iArg];

                if (Directory.Exists(sPictureMap) && Directory.Exists(sPictureMapProcessed))
                {
                    using (SqlConnection myConnection = new SqlConnection(sConnectionString))
                    {
                        try
                        {
                            myConnection.Open();

                            foreach (string sFile in Directory.GetFiles(sPictureMap, "*.jpg"))
                            {
                                try
                                {
                                    string sFileName = Path.GetFileNameWithoutExtension(sFile);
                                    string sCodeArticle = sFileName.Substring(0, 9);
                                    byte[] bImage = File.ReadAllBytes(sFile);
                                    using (System.Data.SqlClient.SqlCommand sqlCommand = myConnection.CreateCommand())
                                    {
                                        sqlCommand.CommandText = "SELECT MAX(GA_ARTICLE) FROM ARTICLE WHERE GA_CODEARTICLE = '" + sCodeArticle + "' AND GA_STATUTART IN ('GEN','UNI')";
                                        string sArticle = (string)sqlCommand.ExecuteScalar();
                                        sqlCommand.CommandText = "SELECT count(*) as total FROM LIENSOLE WHERE LO_TABLEBLOB = 'GA' and LO_QUALIFIANTBLOB = 'PHJ' and LO_EMPLOIBLOB = 'PH1' and LO_IDENTIFIANT = '" + sArticle + "' and LO_RANGBLOB = '0'";
                                        int iCount = (int)sqlCommand.ExecuteScalar();
                                        if (iCount > 0)
                                        {
                                            sqlCommand.CommandText = @"UPDATE LIENSOLE
                                                                        SET LO_LIBELLE       = @FileName
                                                                            ,LO_OBJET        = @Image
                                                                            ,LO_DATEMODIF    = GETDATE()
                                                                        WHERE LO_TABLEBLOB = 'GA' and LO_QUALIFIANTBLOB = 'PHJ' and LO_EMPLOIBLOB = 'PH1' and LO_IDENTIFIANT = '" + sArticle + "' and LO_RANGBLOB = '0'";
                                            sqlCommand.Parameters.AddWithValue("@Image", bImage);
                                            sqlCommand.Parameters.AddWithValue("@FileName", sFileName + ".jpg");
                                            sqlCommand.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            sqlCommand.CommandText = @"INSERT into LIENSOLE
                                                                             (LO_TABLEBLOB
                                                                             ,LO_QUALIFIANTBLOB
                                                                             ,LO_EMPLOIBLOB
                                                                             ,LO_IDENTIFIANT
                                                                             ,LO_RANGBLOB
                                                                             ,LO_LIBELLE
                                                                             ,LO_PRIVE
                                                                             ,LO_OBJET
                                                                             ,LO_DATECREATION
                                                                             ,LO_DATEMODIF
                                                                             ,LO_CREATEUR
                                                                             ,LO_UTILISATEUR)
                                                                            VALUES
                                                                             ('GA'
                                                                             ,'PHJ'
                                                                             ,'PH1'
                                                                             ,@Article
                                                                             ,'0'
                                                                             ,@FileName
                                                                             ,'-'
                                                                             ,@Image
                                                                             ,GETDATE()
                                                                             ,GETDATE()
                                                                             ,'BAT'
                                                                             ,'BAT')";

                                            sqlCommand.Parameters.AddWithValue("@Image", bImage);
                                            sqlCommand.Parameters.AddWithValue("@FileName", sFileName + ".jpg");
                                            sqlCommand.Parameters.AddWithValue("@Article", sArticle);
                                            sqlCommand.ExecuteNonQuery();

                                        }
                                        sqlCommand.CommandText = "SELECT GAT_REFARTICLE FROM ARTICLETIERS WHERE GAT_TYPREFARTTIERS = 'CLI' AND GAT_REFTIERS = '15100027' AND " + sArticle + "' = LEN(GAT_ARTICLE,9)";
                                        string sFileRename = (string)sqlCommand.ExecuteScalar();
                                        if (sFileRename.Trim().Length == 0)
                                        {
                                            sFileRename = sFileName + ".jpg";
                                        }
                                        File.Move(sFile, sPictureMapProcessed + "\\" + sFileRename + ".jpg");

                                        StreamWriter sw = new StreamWriter(sLoggingFile);
                                        sw.WriteLine("File " + sFileName + ".jpg processed");

                                        sw.Close();
                                    }
                                    //SELECT GA_ARTICLE FROM ARTICLE WHERE GA_CODEARTICLE = 'artikelnummer uit bestandsnaam' AND GA_STATUTART IN ('GEN','UNI').
                                }
                                catch (Exception ex)
                                {

                                    string sException = "Something went wrong with " + sFile;
                                    Console.WriteLine(sException);
                                    Console.WriteLine(ex.ToString());

                                    StreamWriter sw = new StreamWriter(sLoggingFile);
                                    sw.WriteLine(sException);
                                    sw.WriteLine(ex.ToString());
                                    sw.Close();
                                    try
                                    {
                                        File.Move(sFile, sPictureMapRejected + "\\" + Path.GetFileNameWithoutExtension(sFile) + ".jpg");
                                    }
                                    catch (Exception ex2)
                                    {
                                        string sException2 = "Move to rejected also failed";
                                        Console.WriteLine(sException);
                                        Console.WriteLine(ex2.ToString());

                                        sw = new StreamWriter(sLoggingFile);
                                        sw.WriteLine(sException2);
                                        sw.WriteLine(ex2.ToString());
                                        sw.Close();
                                    }
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            string sException = "Could not open DB connection";
                            Console.WriteLine(sException);
                            Console.WriteLine(ex.ToString());
                            StreamWriter sw = new StreamWriter(sLoggingFile);
                            sw.WriteLine(sException);
                            sw.WriteLine(ex.ToString());
                            sw.Close();

                        }
                    }
                }
                else
                {
                    string sException = "Incorrect location specified";
                    Console.WriteLine(sException);
                }
            }
            else
            {
                string sException = "Integer argument needs to be specified";
                Console.WriteLine(sException);
            }
        }
    }

}
